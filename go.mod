module go.jolheiser.com/drone-arc

go 1.15

require (
	github.com/mholt/archiver/v3 v3.4.0
	github.com/urfave/cli/v2 v2.2.0
	go.jolheiser.com/beaver v1.0.2
)

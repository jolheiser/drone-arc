package main

import (
	"github.com/mholt/archiver/v3"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
	"os"
	"path/filepath"
)

func main() {
	app := cli.NewApp()
	app.Name = "drone-arc"
	app.Description = "Archive files"
	app.Action = doMain
	app.Flags = []cli.Flag{
		&cli.StringSliceFlag{
			Name:    "files",
			Usage:   "Files to compress",
			EnvVars: []string{"PLUGIN_FILES"},
			Value:   cli.NewStringSlice("*"),
		},
		&cli.StringFlag{
			Name:    "output",
			Usage:   "Output archive",
			EnvVars: []string{"PLUGIN_OUTPUT"},
			Value:   "archive.tar.gz",
		},
	}

	if err := app.Run(os.Args); err != nil {
		beaver.Fatal(err)
	}
}

func doMain(ctx *cli.Context) error {
	files := make([]string, 0)
	for _, glob := range ctx.StringSlice("files") {
		globbed, err := filepath.Glob(glob)
		if err != nil {
			return err
		}
		files = append(files, globbed...)
	}
	return archiver.Archive(files, ctx.String("output"))
}

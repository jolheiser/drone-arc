# Drone Archiver

## Settings

`drone-arc` will determine the archive to use based on `output`

Optional
* `files` - List of file-globs to add to the archive (default: "*")
* `output` - Output name of the archive. (default: "archive.tar.gz")
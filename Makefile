GO ?= go

.PHONY: fmt
fmt:
	$(GO) fmt ./...

.PHONY: build
build:
	$(GO) build

.PHONY: vet
vet:
	$(GO) vet ./...

.PHONY: docker-build
docker-build:
	docker build -t jolheiser/drone-arc .

.PHONY: docker-push
docker-push:
	docker push jolheiser/drone-arc
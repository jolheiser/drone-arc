FROM golang:1.15-alpine as build

RUN apk --no-cache add build-base

WORKDIR /app

COPY . .

RUN make build

FROM alpine:latest
LABEL maintainer="john.olheiser@gmail.com"

COPY --from=build /app/drone-arc /drone-arc

ENTRYPOINT ["/drone-arc"]